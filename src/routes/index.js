import React from "react";
import { Dimensions } from "react-native";
import { createSwitchNavigator, createDrawerNavigator } from "react-navigation";
import SidebarContainer from "../containers/SidebarContainer/SidebarContainer";
import AuthContainer from "../containers/AuthContainer/AuthContainer";
import HomeContainer from "../containers/HomeContainer/HomeContainer";
import SettingContainer from "../containers/SettingContainer/SettingContainer";

const DefaultDrawerNavigator = createDrawerNavigator(
    {
        Home: {
            screen: HomeContainer
        },
        Calendar: {
            screen: HomeContainer
        },
        Overview: {
            screen: HomeContainer
        },
        Groups: {
            screen: HomeContainer
        },
        Lists: {
            screen: HomeContainer
        },
        Profile: {
            screen: HomeContainer
        },
        Timeline: {
            screen: HomeContainer
        },
        Setting: {
            screen: SettingContainer
        },
    },
    {
        drawerWidth: Dimensions.get('window').width,
        headerMode: 'screen',
        contentComponent: (props) => <SidebarContainer {...props}/>,
        drawerBackgroundColor: "rgb(114, 111, 172)",
        contentOptions: {
            activeLabelStyle:{
                color:"#fff",
            },
            activeBackgroundColor: "rgba(114, 111, 172, 0)",
            itemsContainerStyle:{
                paddingHorizontal: 10,
                paddingVertical: 5,
                alignItems: 'center',
            },
            labelStyle:{
                fontSize: 30,
                color: "rgb(174, 175, 223)",
                fontFamily: 'light',
                paddingVertical: 0,
                fontWeight: "normal",
                margin: 10
            }
        }
    },
    
)
const RootStack = createSwitchNavigator(
    {
        Login: {
            screen: AuthContainer
        },
        Home: {
            screen: DefaultDrawerNavigator
        },
        
        Setting: {
            screen: DefaultDrawerNavigator
        },
    },
    {
      initialRouteName: 'Home',
      headerMode: 'none',
    }
);



export default RootStack;