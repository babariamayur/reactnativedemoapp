import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Container, Content, View } from "native-base";
import HeaderContainer from "../../containers/HeaderContainer/HeaderContainer";
import BackgroundImage from "../../components/BackgroundImage/BackgroundImage";


class HomeScreen extends Component {
    render() {
        const { navigation } = this.props;
        return (
            <Container>
                <BackgroundImage source={require("../../assets/images/background/home.jpg")}>
                    <HeaderContainer openDrawer={navigation.openDrawer} />
                    <Content contentContainerStyle={styles.container} bounces={false}>
                        <View style={styles.textContainer}>
                            <Text style={styles.defaultText}>Good Morning!</Text>
                        </View>
                        <View>
                            <View style={styles.circleContainer}>
                                <Text style={styles.circleDateText}>13</Text>
                                <Text style={styles.circleDefaultText}>MONDAY</Text>
                            </View>
                            <View style={styles.badgeContainer}>
                                <Text style={styles.badgeText}>8</Text>
                            </View>
                        </View>
                        <View style={styles.dateContainer}>
                            <Text style={styles.dateText}>AUGUST 2018</Text>
                        </View>
                    </Content>
                </BackgroundImage>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    circleContainer: {
        height: 180,
        width: 180,
        borderRadius: 180,
        backgroundColor: '#50d2c2',
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    circleDateText: {
        color: "#fff",
        fontSize: 65,
        fontFamily: 'regular',
    },
    textContainer: {
        marginBottom: 30,
    },
    defaultText: {
        fontSize: 30,
        fontFamily: "light"
    },
    circleDefaultText: {
        color: "#fff",
        fontSize: 20
    },
    badgeContainer: {
        position: 'absolute',
        top: 8,
        right: 8,
        minWidth: 40,
        height: 40,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#6562a4'
    },
    badgeText: {
        color: "#fff",
        fontSize: 18
    },
    dateContainer:{
        marginTop: 50,
    },
    dateText: {
        fontSize: 25,
        fontFamily: "light"
    }
});

export default HomeScreen;
