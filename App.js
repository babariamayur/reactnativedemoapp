import React from 'react';
import { Font } from "expo";
import Routes from "./src/routes";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded:false
    };
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      'regular': require('./src/assets/fonts/Roboto/Roboto-Regular.ttf'),
      'light': require('./src/assets/fonts/Roboto/Roboto-Light.ttf'),
      'thin': require('./src/assets/fonts/Roboto/Roboto-Thin.ttf'),
    });
    this.setState({ fontLoaded: true });
  }
  render() {
    if(this.state.fontLoaded){
      return (<Routes />);
    }
    return (null)
  }
}
 
export default App;
